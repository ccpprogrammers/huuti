# Huuti LTD

## How to use

### Requirements
- [Node](https://nodejs.org/en/)
- [npm]() - Gets installed with Node
- [gulp](http://gulpjs.com/)

### Setting up the project

clone repository from bitbacket
```
https://bitbucket.org/ccpprogrammers/huuti.git
```

Extract the project zip folder and navigate to the respective folder
```
cd <project-folder>
```
Install the npm packages
```
npm install
```
Start running the gulp tasks
```
gulp watch
aceess site http://localhost:3000
```

to build the application
```
gulp build
```

### Includes
* Bootstrap CDN
* JQery CDN
* Poppins font from Google fonts



